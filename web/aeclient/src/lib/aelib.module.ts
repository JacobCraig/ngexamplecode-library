import { UIRouterModule } from '@uirouter/angular';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home/home.component';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { StudentHomeComponent } from './student-home/student-home.component';
import { StudentDashboardComponent } from './student-dashboard/student-dashboard.component';

@NgModule({
  imports: [
    UIRouterModule,
  ],
  declarations: [
    HomeComponent,
    Page1Component,
    Page2Component,
    StudentHomeComponent,
    StudentDashboardComponent,
  ],
  exports: [
    HomeComponent,
    Page1Component,
    Page2Component,
    StudentHomeComponent,
    StudentDashboardComponent,
  ],
})
export class AelibModule { }
