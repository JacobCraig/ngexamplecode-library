import { Component, OnInit } from '@angular/core';
import { Transition } from '@uirouter/core';

@Component({
  selector: 'aelib-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.scss']
})
export class Page2Component implements OnInit {
  public extra: string;

  constructor(
    public transition: Transition,
  ) {
    this.extra = transition.params().extraParam;
  }

  ngOnInit() {
  }

}
