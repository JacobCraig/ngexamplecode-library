import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Page2Component } from './page2.component';
import { UIRouterModule } from '@uirouter/angular';
import { Transition } from '@uirouter/core';
import { Provider } from '@angular/core';

describe('Page2Component', () => {
  let component: Page2Component;
  let fixture: ComponentFixture<Page2Component>;
  let transition: Transition;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [UIRouterModule],
      declarations: [Page2Component],
      providers: <Provider[]>[
        { provide: Transition, useFactory: () => transition },
      ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    transition = <any>{
      params: function () {
        return {
          extraParam: 'testValue',
        };
      }
    };

    fixture = TestBed.createComponent(Page2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain injected transition', () => {
    expect(component.transition).toBe(transition);
  });
});
