/*
 * Public API Surface of aelib
 */

export * from './lib/aelib.module';
export * from './lib/home/home.component';
export * from './lib/page1/page1.component';
export * from './lib/page2/page2.component';
export * from './lib/student-dashboard/student-dashboard.component';
export * from './lib/student-home/student-home.component';
