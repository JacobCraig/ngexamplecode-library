import { AppComponent } from './app.component';
import { HomeComponent, Page1Component, Page2Component } from '../../../../src/public_api';

export const appState = {
  name: 'app',
  redirectTo: 'home',
  component: AppComponent,
};

export const homeState = {
  name: 'home',
  url: '/',
  component: HomeComponent,
};

export const page1State = {
  name: 'page1',
  url: '/pageUno',
  component: Page1Component,
};

export const page2State = {
  name: 'page2',
  url: '/page2?extraParam',
  component: Page2Component,
};

export const APPSTATES = [
  appState,
  homeState,
  page1State,
  page2State,
];
