import { UIRouter } from '@uirouter/core';

export function routerConfigFn(router: UIRouter) {
    router.urlService.config.caseInsensitive(true);
}
