import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';

import { APPSTATES } from './app.routing';
import { routerConfigFn } from './app.config';
import { AppComponent } from './app.component';
import { AelibModule } from '../../../../src/public_api';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    UIRouterModule.forRoot({
      states: APPSTATES,
      useHash: true,
      otherwise: { state: 'studentHome' },
      config: routerConfigFn,
    }),
    BrowserModule,
    AelibModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
