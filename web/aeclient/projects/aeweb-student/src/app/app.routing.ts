import { AppComponent } from './app.component';
import { StudentDashboardComponent, StudentHomeComponent } from '../../../../src/public_api';

export const appState = {
  name: 'app',
  redirectTo: 'studentHome',
  component: AppComponent,
};

export const homeState = {
  name: 'studentHome',
  url: '/home',
  component: StudentHomeComponent,
};

export const dashboardState = {
  name: 'studentDashboard',
  url: '/dashboard',
  component: StudentDashboardComponent,
};

export const APPSTATES = [
  appState,
  homeState,
  dashboardState,
];
